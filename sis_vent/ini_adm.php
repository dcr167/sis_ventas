<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="css/fontello.css">
    <link rel="stylesheet" type="text/css" href="css/ini_adm.css">
    <link rel="stylesheet" type="text/css" href="css/inicio.css">
    <title>|Inicio|</title>
</head>
<body>
        <header>
                <div class="contenedor">
                  <h1 class="icon-handshake-o"></h1>
                  <h1 class="icon-laptop" >DMC Store</h1>
                  <input type="checkbox" name="add" id="menu-bar" value="">
                  <label class="icon-th-list" for="menu-bar"></label>
                  <nav class="menu">
                    <a href="ini_adm.php" class="icon-laptop">Inicio</a>
                    <a href="#" class="icon-money">Ventas</a>
                    <a href="m_clien.php" class="icon-user-1">Clientes</a>
                    <a href="m_prod.php" class="icon-desktop">Productos</a>
                    <a href="#" class="icon-phone-squared">Contactanos</a>
                  </nav>
                </div>
              </header>
              <main>
                <section id="bnn">
                  <div class="contenedor">
                    <h2>Mejor administracion y atencion</h2>
                    <p>Mejora tu organizacion</p>
                    <center><a href="#">Leer mas</a></center>
                  </div>
                </section>
              </main>
          
    
</body>
</html>