<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>|Inicia Sesion|</title>
    <link rel="stylesheet" type="text/css" href="css/log_pass_admin.css">
</head>
<body>
    <div class="box">
        <h2>Inicia Sesion</h2>
        <form action="#">
            <div class="inputbox">
                <input type="text" name="user_admin" required>
                <label for="usuario">Usuario</label>
            </div>
            <div class="inputbox">
                    <input type="password" name="pass_admin" required>
                    <label for="contraseña">Contraseña</label>
            </div>
            <input type="submit" name="#" value="Entrar">

        </form>
        
    </div>
    
</body>
</html>