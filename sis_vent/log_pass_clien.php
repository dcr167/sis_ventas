<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>|Inicia Sesion|</title>
    <link rel="stylesheet" type="text/css" href="css/log_pass_clien.css">
</head>
<body>
    <div class="box">
        <h2>Inicia Sesion</h2>
        <form action="#">
            <div class="inputbox">
                <input type="text" name="user_clien" required>
                <label for="usuario">Usuario</label>
            </div>
            <div class="inputbox">
                    <input type="password" name="pass_clien" required>
                    <label for="contraseña">Contraseña</label>
            </div>
            <input type="submit" name="#" value="Entrar">

        </form>
        
        <a href="log_pass_reg_clien.php"><p>Crear Cuenta</p></a>
        
    </div>
    
</body>
</html>