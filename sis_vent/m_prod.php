<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" type="text/css" href="css/fontello.css">
    <link rel="stylesheet" type="text/css" href="css/ini_adm.css">
    <link rel="stylesheet" type="text/css" href="css/inicio.css">
    <link rel="stylesheet" type="text/css" href="css/m_clien.css">
    <title>|Productos|</title>
</head>
<body>
        <header>
                <div class="contenedor">
                  <h1 class="icon-handshake-o"></h1>
                  <h1 class="icon-laptop" >DMC Store</h1>
                  <input type="checkbox" name="add" id="menu-bar" value="">
                  <label class="icon-th-list" for="menu-bar"></label>
                  <nav class="menu">
                    <a href="ini_adm.php" class="icon-laptop">Inicio</a>
                    <a href="#" class="icon-money">Ventas</a>
                    <a href="m_clien.php" class="icon-user-1">Clientes</a>
                    <a href="m_prod.php" class="icon-desktop">Productos</a>
                    <a href="#" class="icon-phone-squared">Contactanos</a>
                  </nav>
                </div>
              </header>

              <div class="btn"><a href="reg_prod.php"><p>Registrar Productos</p></a></div>
              <div class="btn"><a href="#"><p>Ver Productos</p></a></div>

            
</body>
</html>