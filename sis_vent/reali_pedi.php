<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>|DMC Store|</title>
    <link rel="stylesheet" type="text/css" href="css/ini_clien.css">
    <link rel="stylesheet" type="text/css" href="css/reali_pedi.css">
</head>
<body>
    <div id="container">
        <ul id="hmenu">
        <li><a href="ini_clien.php">Inicio</a></li>
        <li><a href="#">Pedidos</a>
            <ul class="sub-menu">
                <li><a href="reali_pedi.php">Realizar Pedido</a></li>
            </ul>
        </li>
        <li><a href="#">Productos</a>
            <ul class="sub-menu">
                <li><a href="#">Ver Productos</a></li>
            </ul>
        </li>
        <li><a href="contac.php">Contactanos</a></li>

        </ul>
    </div>
    <div class="from">
            <h1>Realizar Pedido</h1>
            <form action="#" method="POST">
                <label for="codigo">Codigo</label>
                <input type="text" name="codigo">
                <label for="nombre_prod">Nombre</label>
                <input type="text" name="nombre_prod">
                <label for="marca">Marca</label>
                <input type="text" name="marca">
                <label for="modelo">Modelo</label>
                <input type="text" name="modelo">
                <input type="submit" value="Enviar">
            </form>
        </div>   
    
</body>
</html>