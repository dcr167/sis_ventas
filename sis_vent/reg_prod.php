<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>|Productos|</title>
    <link rel="stylesheet" type="text/css" href="css/fontello.css">
    <link rel="stylesheet" type="text/css" href="css/ini_adm.css">
    <link rel="stylesheet" type="text/css" href="css/inicio.css">
    <link rel="stylesheet" type="text/css" href="css/reg_prod.css">
</head>
<body>
        <header>
                <div class="contenedor">
                  <h1 class="icon-handshake-o"></h1>
                  <h1 class="icon-laptop" >DMC Store</h1>
                  <input type="checkbox" name="add" id="menu-bar" value="">
                  <label class="icon-th-list" for="menu-bar"></label>
                  <nav class="menu">
                    <a href="ini_adm.php" class="icon-laptop">Inicio</a>
                    <a href="#" class="icon-money">Ventas</a>
                    <a href="m_clien.php" class="icon-user-1">Clientes</a>
                    <a href="m_prod.php" class="icon-desktop">Productos</a>
                    <a href="#" class="icon-phone-squared">Contactanos</a>
                  </nav>
                </div>
              </header>
              
    <div class="from">
        <h1>Registrar Producto</h1>
        <form action="#" method="POST">
            <label for="codigo">Codigo</label>
            <input type="text" name="codigo">
            <label for="nombre_prod">Nombre</label>
            <input type="text" name="nombre_prod">
            <label for="marca">Marca</label>
            <input type="text" name="marca">
            <label for="modelo">Modelo</label>
            <input type="text" name="modelo">
            <label for="precio">Precio</label>
            <input type="text" name="precio">
            <input type="submit" value="Guardar">
        </form>
    </div>       
    
</body>
</html>