-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-04-2019 a las 05:24:17
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sventdmc`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

CREATE TABLE `clientes` (
  `idc` int(150) NOT NULL,
  `usuarioc` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `nombrec` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `apellidosc` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `correoc` varchar(30) COLLATE utf8_spanish_ci NOT NULL,
  `passwordc` varchar(30) COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`idc`, `usuarioc`, `nombrec`, `apellidosc`, `correoc`, `passwordc`) VALUES
(1, 'Dante', 'Luis', 'Cabrera', 'dan@hotmail.com', '1234567'),
(2, 'Nero', 'Alejandro', 'Cabrera', 'nero@hotmail.com', '4569'),
(3, 'Vergil', 'Alejandro', 'Cabrera', 'nero@hotmail.com', '56892'),
(4, 'Sparda', 'Linux', 'Cabrera', 'sparda@gmail.com', '461945ds');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `clientes`
--
ALTER TABLE `clientes`
  ADD PRIMARY KEY (`idc`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `clientes`
--
ALTER TABLE `clientes`
  MODIFY `idc` int(150) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
